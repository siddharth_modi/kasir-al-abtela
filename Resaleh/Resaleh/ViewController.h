//
//  ViewController.h
//  Resaleh
//
//  Created by siddharth on 10/12/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UIButton *btn_sidemenu;

@property (weak, nonatomic) IBOutlet UITableView *tbl_alldata;

@property (weak, nonatomic) IBOutlet UIView *view_video;

@property (nonatomic, strong) NSMutableArray *selectedTreeItems;

@property (weak, nonatomic) IBOutlet UIImageView *img_video;



@end

