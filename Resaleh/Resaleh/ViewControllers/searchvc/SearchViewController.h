//
//  SearchViewController.h
//  Resaleh
//
//  Created by siddharth on 10/21/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITextField *txt_search;

@property (weak, nonatomic) IBOutlet UIButton *btn_or;
@property (weak, nonatomic) IBOutlet UIButton *btn_and;
@property (weak, nonatomic) IBOutlet UIButton *btn_normal;

@property (weak, nonatomic) IBOutlet UIButton *btn_down;
@property (weak, nonatomic) IBOutlet UIView *view_filter;
@property (weak, nonatomic) IBOutlet UIButton *btn_search;
@property (weak, nonatomic) IBOutlet UIImageView *img_or;
@property (weak, nonatomic) IBOutlet UIImageView *img_and;
@property (weak, nonatomic) IBOutlet UIImageView *img_normal;
@property (weak, nonatomic) IBOutlet UITableView *tbl_Allsearch;


- (IBAction)btn_back:(id)sender;
- (IBAction)btn_search:(id)sender;
- (IBAction)btn_down:(id)sender;

- (IBAction)btn_or:(id)sender;
- (IBAction)btn_and:(id)sender;
- (IBAction)btn_normal:(id)sender;



@end
