//
//  RearViewController.m
//  Resaleh
//
//  Created by siddharth on 10/12/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import "RearViewController.h"
#import "RearTableViewCell.h"
#import "FavouriteViewController.h"
#import "AppDelegate.h"
#import "SearchViewController.h"
#import "MVYSideMenuController.h"
#import "FavouriteViewController.h"
#import "AboutUsViewController.h"
#import "AboutSoftwareViewController.h"
#import "GuideViewController.h"



@interface RearViewController ()
{
    NSArray *arr_name;
    NSArray *icon;
    NSArray *images;
}
@end

@implementation RearViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    arr_name=@[@"جستجوی پیشرفته‎",@"نشان ها",@"درباره ما",@"درباره نرم افزار",@"راهنما"];
    NSLog(@"arryname %@",arr_name);
    
    
    images=@[@"search",@"sign",@"aboutus",@"logo",@"help"];
    
    for (int i=0; i<arr_name.count; i++)
    {
        NSString *newString = [[arr_name objectAtIndex:i] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"%@",newString);
    }
    _tbl_sidemenu.scrollEnabled = NO;

    [_tbl_sidemenu reloadData];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr_name count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RearTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *str=[[arr_name objectAtIndex:indexPath.row] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    cell.lbl_name.text=str;
    cell.img_icon.image=[UIImage imageNamed:[images objectAtIndex:indexPath.row]];
    return cell;
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if (indexPath.row==0)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"pushsearchVC" object:nil];
        
        //        SearchViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
        //
        //        appDelegateLocal.nav = [[UINavigationController alloc] initWithRootViewController:vc];
        //
        //        [[self sideMenuController] changeContentViewController:appDelegateLocal.nav closeMenu:YES];
        
        //        UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:vc];
        //   [self.navigationController pushViewController:appDelegateLocal.nav animated:YES];
        
        //  [appDelegateLocal showsearchView];
    }
    else  if (indexPath.row==1)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"pushfavouriteVC" object:nil];
        
        //        FavouriteViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"FavouriteViewController"];
        //
        //        appDelegateLocal.nav = [[UINavigationController alloc] initWithRootViewController:vc];
        //
        //        [[self sideMenuController] changeContentViewController:appDelegateLocal.nav closeMenu:YES];
    }
    else if (indexPath.row==2)
    {
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"pushAboutUSVC" object:nil];
        
        //        AboutUsViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsViewController"];
        //
        //        appDelegateLocal.nav = [[UINavigationController alloc] initWithRootViewController:vc];
        //
        //        [[self sideMenuController] changeContentViewController:appDelegateLocal.nav closeMenu:YES];
        //  [appDelegateLocal showAboutView];
        
    }
    else if (indexPath.row==3)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"pushAboutSoftwareVC" object:nil];
        //        AboutSoftwareViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"AboutSoftwareViewController"];
        //
        //        appDelegateLocal.nav = [[UINavigationController alloc] initWithRootViewController:vc];
        //
        //        [[self sideMenuController] changeContentViewController:appDelegateLocal.nav closeMenu:YES];
        //   [appDelegateLocal showAboutSoftwareView];
        
    }
    else if (indexPath.row==4)
    {
        //        GuideViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"GuideViewController"];
        //
        //        appDelegateLocal.nav = [[UINavigationController alloc] initWithRootViewController:vc];
        //
        //        [[self sideMenuController] changeContentViewController:appDelegateLocal.nav closeMenu:YES];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"pushguideVC" object:nil];
        
        
        //        GuideViewController *contentVC = [[GuideViewController alloc] init];
        //        UINavigationController *contentNavigationController = [[UINavigationController alloc] initWithRootViewController:contentVC];
        //        [[self sideMenuController] changeContentViewController:contentNavigationController closeMenu:YES];
        //  [appDelegateLocal showGuideView];
        
    }

}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}// Default is 1 if not implemented



@end
