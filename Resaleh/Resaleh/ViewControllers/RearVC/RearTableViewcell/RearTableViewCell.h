//
//  RearTableViewCell.h
//  Resaleh
//
//  Created by siddharth on 10/12/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RearTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *img_icon;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@end
