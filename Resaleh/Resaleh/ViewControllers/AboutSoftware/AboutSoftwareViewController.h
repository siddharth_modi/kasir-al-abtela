//
//  AboutSoftwareViewController.h
//  Resaleh
//
//  Created by siddharth on 10/19/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutSoftwareViewController : UIViewController
- (IBAction)btn_back:(id)sender;

@property (weak, nonatomic) IBOutlet UIWebView *webview_aboutussoftware;
@end
