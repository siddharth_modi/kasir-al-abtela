//
//  AboutSoftwareViewController.m
//  Resaleh
//
//  Created by siddharth on 10/19/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import "AboutSoftwareViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "MVYSideMenuController.h"

@interface AboutSoftwareViewController ()

@end

@implementation AboutSoftwareViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    if (sideMenuController)
    {
        [sideMenuController closeMenu];
    }
    self.navigationController.navigationBarHidden=YES;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"tiledBG"]];

    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"sidemenu"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSString *htmlString=@"<img src='kasirimg.jpeg'>";
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    [_webview_aboutussoftware loadHTMLString:htmlString baseURL:baseURL];
    
    NSString *localFilePath = [[NSBundle mainBundle] pathForResource:@"AboutSoftware" ofType:@"htm"] ;
    NSURLRequest *localRequest = [NSURLRequest requestWithURL:
                                  [NSURL fileURLWithPath:localFilePath]];
    
    [_webview_aboutussoftware loadRequest:localRequest];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_back:(id)sender
{
//    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
//    [appDelegateLocal showHomeView];
//    [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"sidemenu"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    ViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//    
//    [self.navigationController pushViewController:vc animated:YES];
    [self.navigationController popViewControllerAnimated:YES];


}
@end
