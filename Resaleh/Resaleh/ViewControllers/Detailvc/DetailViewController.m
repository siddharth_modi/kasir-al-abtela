//
//  DetailViewController.m
//  Resaleh
//
//  Created by siddharth on 10/18/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import "DetailViewController.h"
#import "AppDelegate.h"
#import <sqlite3.h>
#import <SVProgressHUD.h>
#import "MVYSideMenuController.h"
#import "ViewController.h"


@interface DetailViewController ()<UIAlertViewDelegate>
{
    sqlite3 *database;
    NSString *comment;
    NSString *str_Metadata;
    NSString *LeftswipeNull;
    NSString *RightswipeNull;

    BOOL flag;
}
@end

@implementation DetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"tiledBG"]];

    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"sidemenu"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    LeftswipeNull=@"";
    RightswipeNull=@"";
    [self check_FavouriteOrNot:_str_id];
    _str_favourite=[self FetchData:[NSString stringWithFormat:@"SELECT * FROM WebSite_MetaDataDB where CategoryID=%@",_str_id]];
    
    
    [self set_Data:_str_name :comment];
   _btn_search.layer.cornerRadius=_btn_search.frame.size.height/2;
    _btn_search.layer.masksToBounds=YES;
    
   
//    NSNumberFormatter *formatter = [NSNumberFormatter new];
//    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
//    for (NSInteger i = 0; i < 10; i++)
//    {
//        NSNumber *num = @(i);
//        _str_name = [_str_name stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
//    }
//    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
//    for (NSInteger i = 0; i < 10; i++)
//    {
//        NSNumber *num = @(i);
//        comment = [comment stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
//    }
//    NSString *aux = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue; font-size: 17\">%@</span>",comment];
//    
//    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[aux dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
//    _txt_detailView.attributedText=attrStr;
//    _txt_detailView.textAlignment=NSTextAlignmentRight;
//    _lbl_navname.text=_str_name;
    
    NSLog(@" label text count== %lu",(unsigned long)_str_name.length);
    
//    if (_str_name.length<=30)
//    {
//        [_lbl_navname setFont:[UIFont fontWithName:[NSString stringWithFormat:@"System Bold"] size:17]];
//        
//    }
    
    self.txt_detailView.userInteractionEnabled = YES;
    
    self.txt_detailView.editable = NO;
    
//    if ([_str_flag isEqualToString:@""])
//    {
        UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
        swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
        [self.view addGestureRecognizer:swipeleft];
        
        UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
        swiperight.direction=UISwipeGestureRecognizerDirectionRight;
        [self.view addGestureRecognizer:swiperight];
  //  }

   
    

//    NSLog(@"%@",_str_favourite);
//    
//    if ([_str_favourite isEqualToString:@"0"])
//    {
//        [_btn_favourite setImage:[UIImage imageNamed:@"unfavourite"] forState:UIControlStateNormal];
//        flag=NO;
//
//    }
//    else
//    {
//        [_btn_favourite setImage:[UIImage imageNamed:@"BookMark"] forState:UIControlStateNormal];
//        flag=YES;
//
//    }
    // Do any additional setup after loading the view.
}
-(void)check_FavouriteOrNot:(NSString *)str_Id
{
     _str_favourite=[self FetchData:[NSString stringWithFormat:@"SELECT * FROM WebSite_MetaDataDB where CategoryID=%@",_str_id]];
    NSLog(@"%@",_str_favourite);
    
    if ([_str_favourite isEqualToString:@"0"])
    {
        [_btn_favourite setImage:[UIImage imageNamed:@"unfavourite"] forState:UIControlStateNormal];
        flag=NO;
        
    }
    else
    {
        [_btn_favourite setImage:[UIImage imageNamed:@"BookMark"] forState:UIControlStateNormal];
        flag=YES;
        
    }
}
-(void)set_Data:(NSString *)str_Title :(NSString *)str_Comment
{
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
    for (NSInteger i = 0; i < 10; i++)
    {
        NSNumber *num = @(i);
        str_Title = [str_Title stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
    }
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
    for (NSInteger i = 0; i < 10; i++)
    {
        NSNumber *num = @(i);
        str_Comment = [str_Comment stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
    }
    NSString *aux = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue; font-size: 17\">%@</span>",str_Comment];
    
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[aux dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    _txt_detailView.attributedText=attrStr;
    _txt_detailView.textAlignment=NSTextAlignmentRight;
    _lbl_navname.text=str_Title;
}

#pragma mark - Swipe Left Method height
-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSLog(@" Left swipe ");
   
    
    NSMutableDictionary *Dic_Data=[[NSMutableDictionary alloc] init];
    if (![LeftswipeNull isEqualToString:@"Yes"])
    {
        NSInteger metadataID=[str_Metadata integerValue]-1;
        str_Metadata=[NSString stringWithFormat:@"%ld",(long)metadataID];
    }
   
    
    Dic_Data=[self FetchSwipeData:[NSString stringWithFormat:@"SELECT WebSite_MetaDataDB.CategoryID, WebSite_CategoryDB.Title, WebSite_MetaDataDB.MetaDataID, WebSite_MetaDataDB.Tafsir, WebSite_MetaDataDB.Comment FROM WebSite_MetaDataDB, WebSite_CategoryDB where WebSite_CategoryDB.CategoryID = WebSite_MetaDataDB.CategoryID and  WebSite_MetaDataDB.MetaDataID =%@",str_Metadata]];
    if (Dic_Data.count!=0)
    {
        _str_id=[Dic_Data valueForKey:@"Category_ID"];
        comment=[Dic_Data valueForKey:@"Tafsir"];
        str_Metadata=[Dic_Data valueForKey:@"MetadataID"];
        _str_name=[Dic_Data valueForKey:@"Title"];
        LeftswipeNull=@"";
        [self check_FavouriteOrNot:_str_id];

        [self set_Data:_str_name :comment];
    }
    else
    {
        LeftswipeNull=@"Yes";
        NSInteger metadataID=[str_Metadata integerValue]+1;
        str_Metadata=[NSString stringWithFormat:@"%ld",(long)metadataID];
        
    }
    

    //Do what you want here
}
#pragma mark - Swipe Right Method height
-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    //Do what you want here
    NSLog(@" Right swipe ");
    if (![RightswipeNull isEqualToString:@"Yes"])
    {
        NSInteger metadataID=[str_Metadata integerValue]+1;
        str_Metadata=[NSString stringWithFormat:@"%ld",(long)metadataID];
    }
    NSMutableDictionary *Dic_Data=[[NSMutableDictionary alloc] init];
    
    Dic_Data=[self FetchSwipeData:[NSString stringWithFormat:@"SELECT WebSite_MetaDataDB.CategoryID, WebSite_CategoryDB.Title, WebSite_MetaDataDB.MetaDataID, WebSite_MetaDataDB.Tafsir, WebSite_MetaDataDB.Comment FROM WebSite_MetaDataDB, WebSite_CategoryDB where WebSite_CategoryDB.CategoryID = WebSite_MetaDataDB.CategoryID and  WebSite_MetaDataDB.MetaDataID =%@",str_Metadata]];
    
    if (Dic_Data.count!=0)
    {
        _str_id=[Dic_Data valueForKey:@"Category_ID"];
        comment=[Dic_Data valueForKey:@"Tafsir"];
        str_Metadata=[Dic_Data valueForKey:@"MetadataID"];
        _str_name=[Dic_Data valueForKey:@"Title"];
        RightswipeNull=@"";
        [self check_FavouriteOrNot:_str_id];

        [self set_Data:_str_name :comment];
        
    }
    else
    {
        RightswipeNull=@"Yes";
        NSInteger metadataID=[str_Metadata integerValue]-1;
        str_Metadata=[NSString stringWithFormat:@"%ld",(long)metadataID];
    }



}
#pragma mark - Get label height

-(CGSize)getLabelSize :(UILabel *)label
{
    CGSize size = [label.text boundingRectWithSize:CGSizeMake(203, MAXFLOAT)
                   
                                           options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName: label.font }context:nil].size;
    return size;
}

#pragma mark - Fetch All Data
-(NSString *)FetchData:(NSString *)querySQL
{
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];

    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
    NSString *str_result=[[NSString alloc] init];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        
    {

        NSLog(@"%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
        
        sqlite3_stmt *statement;
        if( sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
            if( sqlite3_step(statement) == SQLITE_ROW )
            {
                char *fieldMetadata = (char *) sqlite3_column_text(statement,0);
                str_Metadata = [[NSString alloc] initWithUTF8String: fieldMetadata];
                char *field1 = (char *) sqlite3_column_text(statement,30);
                str_result = [[NSString alloc] initWithUTF8String: field1];
                
                char *field2 = (char *) sqlite3_column_text(statement,3);
                comment = [[NSString alloc] initWithUTF8String: field2];
                [SVProgressHUD dismiss];
            }
            else
            {
                [SVProgressHUD dismiss];

                UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"My Alert" message:@"Data is not Found" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction
                                               actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                               style:UIAlertActionStyleCancel
                                               handler:^(UIAlertAction *action)
                                               {
                                                   NSLog(@"Cancel action");
                                               }];
                
                UIAlertAction *okAction = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
                                               NSLog(@"OK action");
                                           }];
                
                [alert addAction:cancelAction];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        else
        {
            
            [SVProgressHUD dismiss];

            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
    }
    return str_result;
}

#pragma mark - Fetch Swipe Data
-(NSMutableDictionary *)FetchSwipeData:(NSString *)querySQL
{
//    [SVProgressHUD show];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];

    
    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        
    {

        NSLog(@"%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
        
        sqlite3_stmt *statement;
        if( sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
           // [SVProgressHUD dismiss];

            
            if( sqlite3_step(statement) == SQLITE_ROW )
            {
                NSString *str_Category;
                NSString *str_Title;
                NSString *str_MetadataID;
                NSString *str_Tafsir;
                
                char *fieldCategory = (char *) sqlite3_column_text(statement,0);
                str_Category = [[NSString alloc] initWithUTF8String: fieldCategory];
                char *fieldTitle = (char *) sqlite3_column_text(statement,1);
                str_Title = [[NSString alloc] initWithUTF8String: fieldTitle];
                char *fieldMetadata = (char *) sqlite3_column_text(statement,2);
                str_MetadataID = [[NSString alloc] initWithUTF8String: fieldMetadata];
                char *field1 = (char *) sqlite3_column_text(statement,3);
                str_Tafsir = [[NSString alloc] initWithUTF8String: field1];
                
             
                [dic setValue:str_Category forKey:@"Category_ID"];
                [dic setValue:str_Title forKey:@"Title"];
                [dic setValue:str_MetadataID forKey:@"MetadataID"];
                [dic setValue:str_Tafsir forKey:@"Tafsir"];
            }
        }
        else
        {
            
         //   [SVProgressHUD dismiss];
            
            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
    }
    return dic;
}
#pragma mark - Update Favourite
-(void )UpdateData:(NSString *)querySQL
{
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];

    
    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        
    {
        
        NSLog(@"%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
        
        sqlite3_stmt *statement;
        if( sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
            
            if (sqlite3_step(statement) != SQLITE_DONE)
            {
                
            }
            else
            {
                [SVProgressHUD dismiss];

                NSLog(@"Updated");
            }
        }
        else
        {
            
            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database));
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Button Favourite Method

- (IBAction)btn_favourite:(id)sender
{
    if (flag ==NO)
    {
        [_btn_favourite setImage:[UIImage imageNamed:@"BookMark"] forState:UIControlStateNormal];
        [self UpdateData:[NSString stringWithFormat:@"UPDATE WebSite_MetaDataDB SET favorite='1' WHERE CategoryID=%@",_str_id]];
        flag=YES;
    }
    else
    {
        [_btn_favourite setImage:[UIImage imageNamed:@"unfavourite"] forState:UIControlStateNormal];
        [self UpdateData:[NSString stringWithFormat:@"UPDATE WebSite_MetaDataDB SET favorite='0' WHERE CategoryID=%@",_str_id]];
        flag=NO;
    }
}
#pragma mark - Button Back Method

- (IBAction)btn_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Button Share Method

- (IBAction)btn_share:(id)sender
{
//    
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"نحوه به اشتراک گذاری آیه مورد نظر را انتخاب کنید" preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"اشتراک گذاری با دیگران" style:UIAlertActionStyleDefault handler:nil];
// 
//    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"افزودن به پژوهشیار" style:UIAlertActionStyleDefault handler:nil];
//    
//    [alertController addAction:ok];
//      [alertController addAction:cancel];
//    
//    [self presentViewController:alertController animated:YES completion:nil];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@""
                                                   message:@"نحوه به اشتراک گذاری آیه مورد نظر را انتخاب کنید"
                                                  delegate:self
                                         cancelButtonTitle:@"اشتراک گذاری با دیگران"
                                         otherButtonTitles:@"افزودن به پژوهشیار", nil];
    [alert show];
}
#pragma mark - AlertView Delegate Method

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
        {
            NSString *test = [NSString stringWithFormat:@"%@ \n %@",_lbl_navname.text ,_txt_detailView.text];
            
            NSString *webSite=@"www.makarem.ir";
            
            NSArray *objToShare = @[test,webSite];
            
            UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:objToShare applicationActivities:nil];
            
            activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            
            [self presentViewController:activityViewController animated:YES completion:nil];
        }
        break;
        case 1:
        {
           
        }
            // Do something for button #2
            break;
    }
}

@end
