//
//  DetailViewController.h
//  Resaleh
//
//  Created by siddharth on 10/18/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
@property(nonatomic,retain)NSString *str_id;
@property(nonatomic,retain)NSString *str_name;

@property(nonatomic,retain)NSString *str_favourite;
@property(nonatomic,retain)NSString *str_Comment;
@property(nonatomic,retain)NSString *str_flag;

- (IBAction)btn_share:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btn_favourite;
- (IBAction)btn_favourite:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_details;
@property (weak, nonatomic) IBOutlet UILabel *lbl_navname;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_height;
@property (weak, nonatomic) IBOutlet UIButton *btn_search;
@property (weak, nonatomic) IBOutlet UITextView *txt_detailView;

- (IBAction)btn_back:(id)sender;
@end
