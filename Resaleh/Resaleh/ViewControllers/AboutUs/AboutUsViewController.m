//
//  AboutUsViewController.m
//  Resaleh
//
//  Created by siddharth on 10/19/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import "AboutUsViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "MVYSideMenuController.h"

@interface AboutUsViewController ()

@end

@implementation AboutUsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    if (sideMenuController)
    {
        [sideMenuController closeMenu];
    }
    self.navigationController.navigationBarHidden=YES;
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"sidemenu"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"tiledBG"]];

    NSString *localFilePath = [[NSBundle mainBundle] pathForResource:@"AboutUs" ofType:@"htm"] ;
    NSURLRequest *localRequest = [NSURLRequest requestWithURL:
                                  [NSURL fileURLWithPath:localFilePath]] ;
    
    [_webviewAboutus loadRequest:localRequest];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btn_back:(id)sender
{
//    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
//    [appDelegateLocal showHomeView];
//    [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"sidemenu"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    ViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//    
//    [self.navigationController pushViewController:vc animated:YES];
    [self.navigationController popViewControllerAnimated:YES];

}
@end
