//
//  AppDelegate.m
//  Resaleh
//
//  Created by siddharth on 10/12/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "MVYSideMenuController.h"
#import "RearViewController.h"
#import "FavouriteViewController.h"
#import "AboutUsViewController.h"
#import "AboutSoftwareViewController.h"
#import "GuideViewController.h"
#import "SearchViewController.h"


@interface AppDelegate ()

@end

@implementation AppDelegate

+(AppDelegate *)sharedAppDelegate
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Create your menu view controller
    // Create your main content view controller
    
    [self showHomeView];
    NSString *greeting = NSLocalizedString(@"Hello, World!", @"A friendly greeting");
    NSLog(@"%@", greeting);
    return YES;
}

-(void)showFavouriteView
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FavouriteViewController *menuVC =[storyboard instantiateViewControllerWithIdentifier:@"FavouriteViewController"]; ;
    
    UINavigationController *startNVC = [[UINavigationController alloc] initWithRootViewController:menuVC];
    
    self.window.rootViewController = startNVC;
    
    [self.window makeKeyAndVisible];
}
-(void)showAboutView
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AboutUsViewController *menuVC =[storyboard instantiateViewControllerWithIdentifier:@"AboutUsViewController"]; ;
    
    UINavigationController *startNVC = [[UINavigationController alloc] initWithRootViewController:menuVC];
    
    
    self.window.rootViewController = startNVC;
    
    [self.window makeKeyAndVisible];
}
-(void)showGuideView
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    GuideViewController *menuVC =[storyboard instantiateViewControllerWithIdentifier:@"GuideViewController"]; ;
    
    UINavigationController *startNVC = [[UINavigationController alloc] initWithRootViewController:menuVC];
    
    
    self.window.rootViewController = startNVC;
    
    [self.window makeKeyAndVisible];
}
-(void)showAboutSoftwareView
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AboutSoftwareViewController *menuVC =[storyboard instantiateViewControllerWithIdentifier:@"AboutSoftwareViewController"]; ;
    
    UINavigationController *startNVC = [[UINavigationController alloc] initWithRootViewController:menuVC];
    

    self.window.rootViewController = startNVC;
    
    [self.window makeKeyAndVisible];
}
-(void)showsearchView
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchViewController *menuVC =[storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"]; ;
    
    UINavigationController *startNVC = [[UINavigationController alloc] initWithRootViewController:menuVC];
    self.window.rootViewController = startNVC;
    [self.window makeKeyAndVisible];
}
-(void)showHomeView
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    RearViewController *menuVC =[storyboard instantiateViewControllerWithIdentifier:@"RearViewController"]; ;
    
    ViewController *contentVC = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Override point for customization after application launch.
    
    [self copydatabaseifneeded];
    UINavigationController *contentNavigationController = [[UINavigationController alloc] initWithRootViewController:contentVC];
    MVYSideMenuOptions *options = [[MVYSideMenuOptions alloc] init];
    options.contentViewScale = 1.0;
    options.contentViewOpacity = 0.05;
    options.shadowOpacity = 0.0;
    MVYSideMenuController *sideMenuController = [[MVYSideMenuController alloc] initWithMenuViewController:menuVC
                                                                                    contentViewController:contentNavigationController
                                                                                                  options:options];
    sideMenuController.menuFrame = CGRectMake(0, 60.0, 180.0, self.window.bounds.size.height - 64.0);
    
    self.window.rootViewController = sideMenuController;
    [self.window makeKeyAndVisible];
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
-(void)copydatabaseifneeded
{
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSError *error;
    NSString *dbpath = [self getdatabase];
    
    BOOL success = [filemgr fileExistsAtPath:dbpath];
    
    if (!success)
    {
        NSString *defaultpath = [[[NSBundle mainBundle]resourcePath]stringByAppendingPathComponent:@"kasir.db"];
        success = [filemgr copyItemAtPath:defaultpath toPath:dbpath error:&error];
        
        if (!success)
        {
            NSAssert1(0, @"Fail to create writable database file with message '%@'.", [error localizedDescription]);
        }
    }
    
}
-(NSString *)getdatabase
{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentdir = [path objectAtIndex:0];
    return [documentdir stringByAppendingPathComponent:@"kasir.db"];
}



@end
